#include "ofApp.h"

// test
//--------------------------------------------------------------
void ofApp::setup(){ 

    ofBackground(0);
    ofSetFrameRate(24);
    ofSetVerticalSync(true);
    ofDirectory dir("video");
    dir.allowExt("mp4");
    dir.listDir();
    for (int i = 0; i < dir.size(); i++){
        fileVec.push_back(dir.getPath(i));
    }; 
    file = ofApp::getFile();
    movie.load(file);
    movie.setLoopState(OF_LOOP_NONE);
    movie.play();
    rate = 1.0;
    prevRate = 1.0;
    rateAlpha = 0.9;
    opacity = 0;
    minLength = 10;
    maxLength = 40;
    videoDuration = ofApp::getVideoDuration();
    startTime = ofGetElapsedTimef();
}

//--------------------------------------------------------------
void ofApp::update(){

    if ((ofGetElapsedTimef() - startTime) > videoDuration ){
        file = ofApp::getFile();
        videoDuration = ofApp::getVideoDuration();
        startTime = ofGetElapsedTimef();
        movie.load(file);
        ofLog()<<volume<<endl;
        movie.play();
    };

    movie.setVolume(volume);
    movie.update(); 
}


//--------------------------------------------------------------
void ofApp::draw(){
    movie.draw(0, 0); 
}

string ofApp::getFile(){
    file = fileVec[ofRandom(fileVec.size())]; 
    return file; 
}

float ofApp::getVideoDuration(){
    videoDuration = ofRandom(minLength, maxLength);
    return videoDuration;
}

void ofApp::keyPressed(int key){
    switch (key){
        case 'q':
        case 'Q':
            ofExit();
    }
}

