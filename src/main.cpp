//#include "ofMain.h"
//#include "ofApp.h"

////========================================================================
//int main(int argc, char **argv){
//	ofSetupOpenGL(1024,768,OF_FULLSCREEN);			// <-------- setup the GL context
//	ofRunApp(new ofApp());
//}
//
#include "ofMain.h"
#include "ofApp.h"

//========================================================================
int main(int argc, char **argv){
    ofSetupOpenGL(1024,768,OF_FULLSCREEN);      // <-------- setup the GL context

    float volume_;

    try
    {
        // Define the command line object.
        CmdLine cmd("set volume");

        // Define a value argument and add it to the command line.
        // h/help is already taken
        ValueArg<std::float_t> volumeArg("v", "volume", "host", false, 0.05, "float");
        cmd.add(volumeArg);

        cmd.parse(argc, argv);

        volume_ = volumeArg.getValue();

    }
    catch (ArgException &e) // catch any exceptions
    {
        ofLogError("main") << e.error() << " for arg " << e.argId();
    }

    shared_ptr<ofApp> mainApp(new ofApp);
    mainApp->volume = volume_;
    ofRunApp(mainApp);
} 
