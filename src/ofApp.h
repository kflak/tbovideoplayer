#pragma once

#include "ofMain.h"
#include "ofxTclap.h"

class ofApp : public ofBaseApp{

    public:
        void setup();
        void update();
        void draw();

        void keyPressed(int key);


        ofVideoPlayer movie;
        string file;
        vector<string> fileVec;
        int startFrame;
        float rate;
        float prevRate;
        float rateAlpha;
        int opacity;
        float videoDuration;
        float minLength;
        float maxLength;
        float startTime;
        float volume;


    private: 
        string getFile();
        float getVideoDuration();

};
